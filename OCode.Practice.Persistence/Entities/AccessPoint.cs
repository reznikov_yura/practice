using System;
using System.ComponentModel.DataAnnotations;

namespace OCode.Practice.Persistence.Entities
{
    public class AccessPoint
    {
        public string Title { get; set; }
        [Key]
        public Guid Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime ExpirationTime { get; set; }
    }
}
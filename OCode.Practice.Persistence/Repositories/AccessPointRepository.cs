using System;
using System.Collections.Generic;
using OCode.Practice.Persistence.Data;
using OCode.Practice.Persistence.Entities;
using OCode.Practice.Persistence.Interfaces;

namespace OCode.Practice.Persistence.Repositories
{
    public class AccessPointRepository: IRepository<AccessPoint>
    {
        private readonly AccessPointContext _dataContext;
 
        public AccessPointRepository(AccessPointContext dataContext)
        {
            this._dataContext = dataContext;
        }
        
        public IEnumerable<AccessPoint> GetAll()
        {
            return _dataContext.AccessPoints;
        }
        
        public AccessPoint GetById(Guid id)
        {
            return _dataContext.AccessPoints.Find(id);
        }
        
        public void Create(AccessPoint accessPoint)
        {
            _dataContext.AccessPoints.Add(accessPoint);
        }
        
        public void Update(AccessPoint newAccessPoint)
        {
            _dataContext.Update(newAccessPoint);
        }
        
        public void Delete(Guid id)
        {
            var accessPoint = _dataContext.AccessPoints.Find(id);
            if(accessPoint != null)
                _dataContext.AccessPoints.Remove(accessPoint);
        }
        
        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
using Microsoft.EntityFrameworkCore;
using OCode.Practice.Persistence.Entities;

namespace OCode.Practice.Persistence.Data
{
    public class KeyCardContext : DbContext
    {
        public DbSet<KeyCardData> KeyCardData { get; set; }

        public KeyCardContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=10.10.3.188;" +
                                     "Port=5432;" +
                                     "Database=intershipdb;" +
                                     "Username=intershipuser;" +
                                     "Password=Pa$$w0rd");
        }
    }
}

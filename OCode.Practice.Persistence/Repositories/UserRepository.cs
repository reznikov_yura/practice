using System;
using System.Collections.Generic;

namespace OCode.Practice.Persistence
{
    public class UserRepository : IRepository<User>
    {
        private readonly UserContext _database;

        public UserRepository(UserContext _database)
        {
            this._database = _database;
        }

        public IEnumerable<User> GetAll()
        {
            return _database.Users;
        }

        public User GetById(Guid id)
        {
            return _database.Users.Find(id);
        }

        public void Create(User user)
        {
            _database.Users.Add(user);
        }

        public void Update(User user)
        {
            _database.Update(user);
        }

        public void Delete(Guid id)
        {
            var user = _database.Users.Find(id);
            if (user != null)
                _database.Users.Remove(user);
        }

        public void Save()
        {
            _database.SaveChanges();
        }
    }
}

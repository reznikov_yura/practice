using System;
using System.Linq;
using System.Collections.Generic;
using OCode.Practice.Persistence.Entities;
using OCode.Practice.Persistence.Interfaces;
using OCode.Practice.Persistence.Data;

namespace OCode.Practice.Persistence.Repositories
{
    public class KeyCardRepository : IRepository<KeyCardData>
    {
        private readonly KeyCardContext _keyCardContext;

        public KeyCardRepository(KeyCardContext keyCardContext)
        {
            _keyCardContext = keyCardContext;
        }

        public void Create(KeyCardData item)
        {
            _keyCardContext.KeyCardData.Add(item);
        }

        public void Delete(Guid id)
        {
            _keyCardContext.KeyCardData.Remove(GetById(id));
        }

        public IEnumerable<KeyCardData> GetAll()
        {
            return _keyCardContext.KeyCardData.ToList();
        }

        public KeyCardData GetById(Guid id)
        {
            return _keyCardContext.KeyCardData.Find(id);
        }

        public void Save()
        {
            _keyCardContext.SaveChanges();
        }

        public void Update(KeyCardData item)
        {
            var itemToUpdate = GetById(item.KeyCardId);
            if (itemToUpdate is null)
            {
                throw new ArgumentException("Provided id is not present in this context");
            }
            else
            {
                itemToUpdate.UserId = item.UserId;
                itemToUpdate.AccesPointId = item.AccesPointId;
                itemToUpdate.StartTimestamp = item.StartTimestamp;
                itemToUpdate.ExpirationTimestamp = item.ExpirationTimestamp;
            }
        }
    }
}

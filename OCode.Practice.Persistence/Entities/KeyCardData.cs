using System;
using System.ComponentModel.DataAnnotations;

namespace OCode.Practice.Persistence.Entities
{
    public class KeyCardData
    {
        [Key]
        public Guid KeyCardId { get; set; }
        public Guid? UserId { get; set; }
        public Guid? AccesPointId { get; set; }
        public DateTime? StartTimestamp { get; set; }
        public DateTime? ExpirationTimestamp { get; set; }

        public KeyCardData() { }

        public KeyCardData(Guid? userId,
                           Guid? accesPointId,
                           DateTime? startTimestamp,
                           DateTime? expirationTimestamp)
        {
            KeyCardId = Guid.NewGuid();
            UserId = userId;
            AccesPointId = accesPointId;
            StartTimestamp = startTimestamp;
            ExpirationTimestamp = expirationTimestamp;
        }
    }
}
